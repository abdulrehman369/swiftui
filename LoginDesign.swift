//
//  LoginDesign.swift
//  SwiftUiPractice
//
//  Created by Admin Admin on 8/13/21.
//

import SwiftUI

struct LoginDesign: View {
    @State var userName : String = ""
    var body: some View {
        VStack{
            
            Text("Welcome To Login Page").padding(.top,20)
                .foregroundColor(.white)
          
            Image("car").resizable()
            .clipped()
            .aspectRatio(contentMode: /*@START_MENU_TOKEN@*/.fill/*@END_MENU_TOKEN@*/)
                .frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/)
            TextField("UserName", text: $userName)
                .padding()
                .background(Color.white)
            cornerRadius(8)
                    .padding(.bottom,20)
                
            
            Spacer()
          
        }.background(Image("nature").resizable().aspectRatio( contentMode: /*@START_MENU_TOKEN@*/.fill/*@END_MENU_TOKEN@*/))
        .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
    
    }
}

struct LoginDesign_Previews: PreviewProvider {
    static var previews: some View {
        LoginDesign()
    }
}
