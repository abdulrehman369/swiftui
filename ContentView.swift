//
//  ContentView.swift
//  SwiftUiPractice
//
//  Created by Admin Admin on 8/12/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            
            MapView().frame( height: 300).edgesIgnoringSafeArea(.top)
            CircleImageView().offset(y:-80)
            
            VStack(alignment:.leading){
            
                Text("Current Location:")
                    .padding().accentColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                    .font(.subheadline)
    
                HStack{
                    Text("Name:")
                        .padding().accentColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                        .font(.subheadline)
                Spacer()
                    Text("Ahmad")
                        .padding().accentColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                        .font(.subheadline)
                }
            }
            .padding(.bottom,10)
            Spacer()
        }
    
       
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
