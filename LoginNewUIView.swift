//
//  LoginNewUIView.swift
//  SwiftUiPractice
//
//  Created by Admin Admin on 8/13/21.
//

import SwiftUI

struct LoginNewUIView: View {
    @State var userName : String = ""
    var body: some View {
        VStack{
            Text("Welcome To Login Page").padding(.top,20)
                .foregroundColor(.white)
          
            Image("car").resizable()
            .clipped()
            .aspectRatio(contentMode: /*@START_MENU_TOKEN@*/.fill/*@END_MENU_TOKEN@*/)
                .frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/)
            
                VStack() {
                           TextField("Email", text: self.$userName)
                               .frame(height: 50).border(Color.red)
                               .textFieldStyle(RoundedBorderTextFieldStyle())
                               .cornerRadius(16)
                               .padding([.leading, .trailing], 24)

                    SecureField("Password", text: self.$userName)
                               .frame(height: 55)
                        .border(Color.red)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .cornerRadius(16)
                               .padding([.leading, .trailing], 24)
                }.padding(.top,30)
            Button(action: {}) {
              Text("Sign In")
                .font(.headline)
                .foregroundColor(.white)
                .padding()
                .frame(width: 300, height: 50)
                .background(Color.green)
                .cornerRadius(15.0)
            }.padding(.top,40)
            VStack(alignment: .trailing) {
                Text("Hello World")
            }
            Spacer()

          
        }
//        .backgrobund(Image("nature").resizable().aspectRatio( contentMode: /*@START_MENU_TOKEN@*/.fill/*@END_MENU_TOKEN@*/))
        .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
    

    }
}

struct LoginNewUIView_Previews: PreviewProvider {
    static var previews: some View {
        LoginNewUIView()
    }
}
