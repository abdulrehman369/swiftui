//
//  CircleImageView.swift
//  SwiftUiPractice
//
//  Created by Admin Admin on 8/12/21.
//

import SwiftUI

struct CircleImageView: View {
    var body: some View {
        Image("car").clipShape(Circle()).overlay(Circle().stroke(Color.gray,lineWidth:3))

            .frame(width: 100, height: 100, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)            .shadow(radius:10)
    }
}

struct CircleImageView_Previews: PreviewProvider {
    static var previews: some View {
        CircleImageView()
    }
}
 
