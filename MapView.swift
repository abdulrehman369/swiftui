//
//  MapView.swift
//  SwiftUiPractice
//
//  Created by Admin Admin on 8/12/21.
//

import SwiftUI
import MapKit
struct MapView: UIViewRepresentable {
    func makeUIView(context: Context) -> some MKMapView {
        MKMapView()
    }
    func updateUIView(_ uiView: UIViewType, context: Context) {
        let coordinate=CLLocationCoordinate2D(latitude: 31.54293, longitude: 74.36044)
        let lahore=MKCoordinateSpan(latitudeDelta: 2.0, longitudeDelta: 2.0)
        let region=MKCoordinateRegion(center: coordinate, span: lahore)
        uiView.setRegion(region, animated: true)
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView()
    }
}
  
